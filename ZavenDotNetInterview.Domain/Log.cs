﻿using System;

namespace ZavenDotNetInterview.Domain
{
    public class Log
    {
        public Log(Guid logId, string description, DateTime currentTime)
        {
            Id = logId;
            Description = description;
            CreatedAt = currentTime;
        }

        protected Log() { }

        public Guid Id { get; private set; }
        public string Description { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public Guid JobId { get; private set; }
        public virtual Job Job { get; private set; }
    }
}