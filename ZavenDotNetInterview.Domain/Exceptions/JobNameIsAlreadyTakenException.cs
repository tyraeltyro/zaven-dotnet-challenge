﻿using System;

namespace ZavenDotNetInterview.Domain.Exceptions
{
    public class JobNameIsAlreadyTakenException: Exception
    {
    }
}
