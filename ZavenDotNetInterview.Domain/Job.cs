﻿using System;
using System.Collections.Generic;
using ZavenDotNetInterview.Domain.Exceptions;

namespace ZavenDotNetInterview.Domain
{
    public class Job
    {
        const int MaxNumberOfFailures = 5;

        public Job(Guid id, string name, DateTime? doAfter, DateTime createDate)
        {
            Id = id;
            Name = name;
            DoAfter = doAfter;
            Status = JobStatus.New;
            Logs = new List<Log>();
            Logs.Add(new Log(Guid.NewGuid(), "Job was created", createDate));
            CreateDate = createDate;
            FailureCount = 0;
        }

        protected Job() { }

        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public JobStatus Status { get; private set; }
        public DateTime? DoAfter { get; private set; }
        public virtual List<Log> Logs { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime? LastUpdatedAt { get; private set; }
        public int FailureCount { get; private set; }

        public void Started()
        {
            if(Status != JobStatus.New && Status != JobStatus.Failed)
            {
                throw new NotAllowedToStartException();
            }
            Logs.Add(new Log(Guid.NewGuid(), "Job was started", DateTime.UtcNow));
            ChangeStatus(JobStatus.InProgress);
        }

        public void Failed()
        {
            FailureCount++;
            if (FailureCount >= MaxNumberOfFailures)
            {
                Logs.Add(new Log(Guid.NewGuid(), "Job processing failed 5 times and was closed", DateTime.UtcNow));
                ChangeStatus(JobStatus.Closed);
            }
            else
            {
                Logs.Add(new Log(Guid.NewGuid(), "Job processing failed", DateTime.UtcNow));
                ChangeStatus(JobStatus.Failed);
            }
        }

        public void Done()
        {
            Logs.Add(new Log(Guid.NewGuid(), "Job processing ended successfully", DateTime.UtcNow));
            ChangeStatus(JobStatus.Done);
        }

        private void ChangeStatus(JobStatus newStatus)
        {
            Status = newStatus;
            LastUpdatedAt = DateTime.UtcNow;
        }
    }

    public enum JobStatus
    {
        Failed = -1,
        New = 0,
        InProgress = 1,
        Done = 2,
        Closed = 3,
    }
}