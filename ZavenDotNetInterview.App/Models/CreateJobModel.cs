﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ZavenDotNetInterview.Application.Command;

namespace ZavenDotNetInterview.App.Models
{
    public class CreateJobModel
    {
        [Required]
        [Remote("IsNameUnique", "Jobs", ErrorMessage = "Job name is taken.")]
        public String Name { get; set; }

        [DisplayName("Do after")]
        [DataType(DataType.Date)]
        public DateTime? DoAfter { get; set; }

        public CreateJobCommand MapToCommand()
        {
            return new CreateJobCommand
            {
                Name = this.Name,
                DoAfter = this.DoAfter,
                JobId = Guid.NewGuid()
            };
        }
    }
}