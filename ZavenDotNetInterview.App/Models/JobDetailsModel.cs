﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.App.Models
{
    public class JobDetailsModel
    {
        public string Name { get; set; }

        public JobStatus Status { get; set; }

        [DisplayName("Process after date")]
        public DateTime? DoAfter { get; set; }

        [DisplayName("Created at")]
        public DateTime CreateDate { get; set; }

        [DisplayName("Last updated at")]
        public DateTime? LastUpdatedAt { get; set; }

        [DisplayName("Failure count")]
        public int FailureCount { get;  set; }

        public List<LogListItemModel> Logs { get; set; }

        internal static JobDetailsModel MapToModel(Job job)
        {
            return new JobDetailsModel
            {
                Name = job.Name,
                Status = job.Status,
                CreateDate = job.CreateDate,
                DoAfter = job.DoAfter,
                FailureCount = job.FailureCount,
                LastUpdatedAt = job.LastUpdatedAt,
                Logs = job.Logs.OrderBy(l=>l.CreatedAt).Select(l => LogListItemModel.MapToModel(l)).ToList()
            };
        }
    }
}