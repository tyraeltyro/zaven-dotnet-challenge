﻿using System;
using System.ComponentModel;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.App.Models
{
    public class LogListItemModel
    {
        public string Description { get; set; }

        [DisplayName("Created at")]
        public DateTime CreatedAt { get; set; }

        internal static LogListItemModel MapToModel(Log log)
        {
            return new LogListItemModel
            {
                Description = log.Description,
                CreatedAt = log.CreatedAt
            };
        }
    }
}