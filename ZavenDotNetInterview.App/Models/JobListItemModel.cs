﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.App.Models
{
    public class JobListItemModel
    {
        public Guid JobId { get; set; }

        public string Name { get; set; }

        [DisplayName("Status")]
        public JobStatus JobStatus { get; set; }

        internal static List<JobListItemModel> MapToModel(List<Job> jobs)
        {
            return jobs.Select(job => new JobListItemModel
            {
                JobId = job.Id,
                Name = job.Name,
                JobStatus = job.Status
            }).ToList();
        }
    }
}