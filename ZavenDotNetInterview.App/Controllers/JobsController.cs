﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ZavenDotNetInterview.App.Models;
using ZavenDotNetInterview.Application.Command;
using ZavenDotNetInterview.Application.Queries;
using ZavenDotNetInterview.Application.Queries.Handlers;
using ZavenDotNetInterview.Application.Services;

namespace ZavenDotNetInterview.App.Controllers
{
    public class JobsController : Controller
    {
        private readonly IJobProcessorService _jobProcessorService;
        private readonly IsJobNameUniqueQueryHandler _isJobNameUniqueQueryHandler;
        private readonly CreateJobCommandHandler _createJobCommandHandler;
        private readonly GetAllJobsQueryHandler _getAllJobsQueryHandler;
        private readonly GetJobDetailsQueryHandler _getJobDetailsQueryHandler;

        public JobsController(
            IJobProcessorService jobProcessorService,
            IsJobNameUniqueQueryHandler isJobNameUniqueQueryHandler,
            CreateJobCommandHandler createJobCommandHandler,
            GetAllJobsQueryHandler getAllJobsQueryHandler, 
            GetJobDetailsQueryHandler getJobDetailsQueryHandler)
        {
            _jobProcessorService = jobProcessorService;
            _isJobNameUniqueQueryHandler = isJobNameUniqueQueryHandler;
            _createJobCommandHandler = createJobCommandHandler;
            _getAllJobsQueryHandler = getAllJobsQueryHandler;
            _getJobDetailsQueryHandler = getJobDetailsQueryHandler;
        }

        // GET: Tasks
        public async Task<ActionResult> Index()
        {
            var jobs = await _getAllJobsQueryHandler.execute(new GetAllJobsQuery());
            return View(JobListItemModel.MapToModel(jobs));
        }

        // POST: Tasks/Process
        [HttpGet]
        public async Task<ActionResult> Process()
        {
            await _jobProcessorService.ProcessJobs();
            return RedirectToAction("Index");
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tasks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateJobModel model)
        {
            if (ModelState.IsValid)
            {
                await _createJobCommandHandler.Execute(model.MapToCommand());
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> Details(Guid jobId)
        {
            var job = await _getJobDetailsQueryHandler.execute(new GetJobDetailsQuery{ JobId = jobId });
            return View(JobDetailsModel.MapToModel(job));
        }

        public async Task<ActionResult> IsNameUnique(string name)
        {
            var isUnique = await _isJobNameUniqueQueryHandler.execute(new IsJobNameUniqueQuery { Name = name });
            return Json(isUnique, JsonRequestBehavior.AllowGet);
        }
    }
}
