﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZavenDotNetInterview.Application.Command
{
    public class CreateJobCommand: ICommand
    {
        public Guid JobId { get; set; }

        public string Name { get; set; }

        public DateTime? DoAfter { get; set; }
    }
}
