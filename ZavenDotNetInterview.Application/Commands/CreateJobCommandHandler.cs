﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using ZavenDotNetInterview.Data.Context;
using ZavenDotNetInterview.Domain;
using ZavenDotNetInterview.Domain.Exceptions;

namespace ZavenDotNetInterview.Application.Command
{
    public class CreateJobCommandHandler : ICommandHandler<CreateJobCommand>
    {
        private readonly ZavenDotNetInterviewContext _context;

        public CreateJobCommandHandler(ZavenDotNetInterviewContext context)
        {
            _context = context;
        }

        public async Task Execute(CreateJobCommand command)
        {
            if (await _context.Jobs.AnyAsync(job => job.Name == command.Name)) throw new JobNameIsAlreadyTakenException();
            Job newJob = new Job(command.JobId, command.Name, command.DoAfter, DateTime.UtcNow);
            _context.Jobs.Add(newJob);
            await _context.SaveChangesAsync();
        }
    }
}
