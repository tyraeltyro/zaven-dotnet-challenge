﻿using System.Threading.Tasks;

namespace ZavenDotNetInterview.Application.Command
{
    interface ICommandHandler<T> where T : ICommand
    {
        Task Execute(T command);
    }
}
