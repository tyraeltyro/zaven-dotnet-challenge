﻿using System.Collections.Generic;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.Application.Queries
{
    public class GetAllJobsQuery: IQuery<List<Job>>
    {
    }
}
