﻿using System;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.Application.Queries
{
    public class GetJobDetailsQuery : IQuery<Job>
    {
        public Guid JobId { get; set; }
    }
}
