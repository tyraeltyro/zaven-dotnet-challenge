﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZavenDotNetInterview.Application.Queries
{
    public class IsJobNameUniqueQuery : IQuery<bool>
    {
        public string Name { get; set; }
    }
}
