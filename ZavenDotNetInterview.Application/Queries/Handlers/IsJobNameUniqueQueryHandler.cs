﻿using System.Data.Entity;
using System.Threading.Tasks;
using ZavenDotNetInterview.Data.Context;

namespace ZavenDotNetInterview.Application.Queries.Handlers
{
    public class IsJobNameUniqueQueryHandler : IQueryHandler<IsJobNameUniqueQuery, bool>
    {
        private readonly ZavenDotNetInterviewContext _context;

        public IsJobNameUniqueQueryHandler(ZavenDotNetInterviewContext context)
        {
            _context = context;
        }

        public async Task<bool> execute(IsJobNameUniqueQuery query)
        {
            return ! await _context.Jobs.AnyAsync(job => job.Name == query.Name);
        }
    }
}
