﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ZavenDotNetInterview.Data.Context;
using ZavenDotNetInterview.Domain;
using ZavenDotNetInterview.Domain.Exceptions;

namespace ZavenDotNetInterview.Application.Queries.Handlers
{
    public class GetJobDetailsQueryHandler : IQueryHandler<GetJobDetailsQuery, Job>
    {
        ZavenDotNetInterviewContext _context;

        public GetJobDetailsQueryHandler(ZavenDotNetInterviewContext context)
        {
            _context = context;
        }

        public async Task<Job> execute(GetJobDetailsQuery query)
        {
            return await _context.Jobs.Include(j => j.Logs).FirstOrDefaultAsync(j => j.Id == query.JobId) ??
                throw new JobNotFoundException();
        }
    }
}
