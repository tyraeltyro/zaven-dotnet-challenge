﻿using System.Threading.Tasks;

namespace ZavenDotNetInterview.Application.Queries.Handlers
{
    interface IQueryHandler<T, Z> where T : IQuery<Z>
    {
        Task<Z> execute(T query);
    }
}
