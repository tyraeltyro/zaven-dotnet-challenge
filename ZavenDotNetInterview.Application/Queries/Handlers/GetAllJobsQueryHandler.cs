﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ZavenDotNetInterview.Data.Context;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.Application.Queries.Handlers
{
    public class GetAllJobsQueryHandler : IQueryHandler<GetAllJobsQuery, List<Job>>
    {
        private readonly ZavenDotNetInterviewContext _context;

        public GetAllJobsQueryHandler(ZavenDotNetInterviewContext context)
        {
            _context = context;
        }

        public async Task<List<Job>> execute(GetAllJobsQuery query)
        {
            return await _context.Jobs.OrderBy(job => job.CreateDate).ToListAsync();
        }
    }
}
