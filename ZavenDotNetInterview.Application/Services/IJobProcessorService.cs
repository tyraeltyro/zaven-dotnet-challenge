﻿using System.Threading.Tasks;

namespace ZavenDotNetInterview.Application.Services
{
    public interface IJobProcessorService
    {
        Task ProcessJobs();
    }
}