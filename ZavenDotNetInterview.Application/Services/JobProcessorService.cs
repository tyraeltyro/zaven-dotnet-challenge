﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ZavenDotNetInterview.Data.Context;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.Application.Services
{
    public class JobProcessorService : IJobProcessorService
    {
        private ZavenDotNetInterviewContext _context;

        public JobProcessorService(ZavenDotNetInterviewContext context)
        {
            _context = context;
        }

        public async Task ProcessJobs()
        {
            _context.Database.Log = Console.WriteLine;
            var currentDate = DateTime.UtcNow.Date;
            var jobsToProcess = await _context.Jobs
                .Include(job => job.Logs)
                .Where(job => (job.Status == JobStatus.New || job.Status == JobStatus.Failed) 
                && (!job.DoAfter.HasValue || job.DoAfter <= currentDate))
                .ToListAsync();

            jobsToProcess.ForEach(job => job.Started());

            await _context.SaveChangesAsync();

            var processingJobs = jobsToProcess.Select(job => UpdateAndProcessJob(job));

            await Task.WhenAll(processingJobs);

            await _context.SaveChangesAsync();
        }

        private async Task UpdateAndProcessJob(Job job)
        {
            bool result = await this.ProcessJob(job).ConfigureAwait(false);
            if (result)
            {
                job.Done();
            }
            else
            {
                job.Failed();
            }
        }

        private async Task<bool> ProcessJob(Job job)
        {
            Random rand = new Random();
            if (rand.Next(10) < 5)
            {
                await Task.Delay(2000);
                return false;
            }
            else
            {
                await Task.Delay(1000);
                return true;
            }
        }
    }
}