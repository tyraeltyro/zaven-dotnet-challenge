﻿using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using ZavenDotNetInterview.Application.Command;
using ZavenDotNetInterview.Data.Context;
using ZavenDotNetInterview.Domain;
using ZavenDotNetInterview.Domain.Exceptions;

namespace ZavenDotNetInterview.Test.CommandsTest
{
    [TestFixture]
    class CreateJobCommandTest
    {
        [TestCase]
        public async Task WhenCreateNewJob_ShouldRunAddMethodeOnRepository_WhenJobIsValid()
        {
            // Give
            var mockLogsSet = GetMockSet(new List<Log>().AsQueryable());
            var mockJobsSet = GetMockSet(new List<Job>().AsQueryable());

            var mockContext = new Mock<ZavenDotNetInterviewContext>();
            mockContext.Setup(m => m.Jobs).Returns(mockJobsSet.Object);
            mockContext.Setup(m => m.Logs).Returns(mockLogsSet.Object);
            var command = new CreateJobCommand { JobId = Guid.NewGuid(), Name = "TestName", DoAfter = DateTime.UtcNow };
            var handler = new CreateJobCommandHandler(mockContext.Object);

            // When 
            await handler.Execute(command);

            // Then
            mockJobsSet.Verify(j => j.Add(It.IsAny<Job>()), Times.Once);
        }

        [TestCase]
        public void WhenCreateNewJob_ShouldThrow_WhenJobIsInvalid()
        {
            // Given
            var testName = "TestName";

            var jobs = new List<Job>
            {
                new Job(Guid.NewGuid(), testName, DateTime.Now.Date, DateTime.Now)

            }.AsQueryable();
            var mockContext = GetMockContext(jobs, new List<Log>().AsQueryable());
            var command = new CreateJobCommand { JobId = Guid.NewGuid(), Name = testName, DoAfter = DateTime.UtcNow };
            var handler = new CreateJobCommandHandler(mockContext.Object);

            // When 
            var exResult = Assert.ThrowsAsync<JobNameIsAlreadyTakenException>(async () => await handler.Execute(command));

            // Then
            exResult.ShouldNotBeNull();
            exResult.ShouldBeOfType(typeof(JobNameIsAlreadyTakenException));
        }

        Mock<ZavenDotNetInterviewContext> GetMockContext(IQueryable<Job> jobs, IQueryable<Log> logs)
        {
            var mockLogsSet = GetMockSet(logs);
            var mockJobsSet = GetMockSet(jobs);

            var mockContext = new Mock<ZavenDotNetInterviewContext>();
            mockContext.Setup(m => m.Jobs).Returns(mockJobsSet.Object);
            mockContext.Setup(m => m.Logs).Returns(mockLogsSet.Object);
            return mockContext;
        }

        Mock<DbSet<T>> GetMockSet<T>(IQueryable<T> data) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();

            mockSet.As<IDbAsyncEnumerable<T>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<T>(data.GetEnumerator()));

            mockSet.As<IQueryable<Job>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<T>(data.Provider));

            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            return mockSet;
        }
    }
}
