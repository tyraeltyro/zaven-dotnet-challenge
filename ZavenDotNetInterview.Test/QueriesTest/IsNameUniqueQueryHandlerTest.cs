﻿
using System.Threading.Tasks;
using ZavenDotNetInterview.Application.Queries;
using Shouldly;
using NUnit.Framework;
using ZavenDotNetInterview.Data.Context;
using ZavenDotNetInterview.Domain;
using System;
using ZavenDotNetInterview.Application.Queries.Handlers;
using Moq;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Infrastructure;

namespace ZavenDotNetInterview.Test.QueriesTest
{
    [TestFixture]
    public class IsNameUniqueQueryHandlerTest
    {

        [TestCase]
        public async Task IsJobNameUnique_ShouldReturnFalse_WhenNameIsNotUnique()
        {
            // Given 
            var testName = "TestName";

            var jobs = new List<Job>
            {
                new Job(Guid.NewGuid(), testName, DateTime.Now.Date, DateTime.Now)

            }.AsQueryable();
            var mockContext = GetMockContext(jobs, new List<Log>().AsQueryable());

            var query = new IsJobNameUniqueQuery{ Name = testName };
            var queryHandler = new IsJobNameUniqueQueryHandler(mockContext.Object);

            // When
            var result = await queryHandler.execute(query);

            // Then
            result.ShouldBe(false);
        }

        [TestCase]
        public async Task IsJobNameUnique_ShouldReturnTrue_WhenNameIsUnique()
        {
            // Given 
            var testName = "TestName";
            var mockContext = GetMockContext(new List<Job>().AsQueryable(), new List<Log>().AsQueryable());
            var query = new IsJobNameUniqueQuery { Name = testName };
            var queryHandler = new IsJobNameUniqueQueryHandler(mockContext.Object);

            // When
            var result = await queryHandler.execute(query);

            // Then
            result.ShouldBe(true);
        }

        Mock<ZavenDotNetInterviewContext> GetMockContext(IQueryable<Job> jobs, IQueryable<Log> logs)
        {
            var mockLogsSet = GetMockSet(logs);
            var mockJobsSet = GetMockSet(jobs);

            var mockContext = new Mock<ZavenDotNetInterviewContext>();
            mockContext.Setup(m => m.Jobs).Returns(mockJobsSet.Object);
            mockContext.Setup(m => m.Logs).Returns(mockLogsSet.Object);
            return mockContext;
        }

        Mock<DbSet<T>> GetMockSet<T>(IQueryable<T> data) where T: class
        {
            var mockSet =  new Mock<DbSet<T>>();

            mockSet.As<IDbAsyncEnumerable<T>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<T>(data.GetEnumerator()));

            mockSet.As<IQueryable<Job>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<T>(data.Provider));

            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            return mockSet;
        }
    }
}