﻿using System.Data.Common;
using System.Data.Entity;
using ZavenDotNetInterview.Domain;

namespace ZavenDotNetInterview.Data.Context
{
    public class ZavenDotNetInterviewContext : DbContext
    {
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<Log> Logs { get; set; }

        public ZavenDotNetInterviewContext() : base("name=ZavenDotNetInterview")
        {
        }
    }
}