﻿namespace ZavenDotNetInterview.App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFailureCountToJob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "FailureCount", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "FailureCount");
        }
    }
}
