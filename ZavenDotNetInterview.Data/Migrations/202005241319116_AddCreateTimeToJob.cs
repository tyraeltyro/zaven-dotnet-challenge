﻿namespace ZavenDotNetInterview.App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreateTimeToJob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "CreateDate", c => c.DateTime(nullable: false, defaultValue: DateTime.UtcNow));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "CreateDate");
        }
    }
}
